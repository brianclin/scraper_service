package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/h2non/gock"
	"github.com/prometheus/common/expfmt"
	"github.com/stretchr/testify/assert"
)

func TestPromRouter(t *testing.T) {
	router := promRouter()

	w := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodGet, "/metrics", nil)
	assert.Nil(t, err)

	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
}

func TestRouter(t *testing.T) {
	router := router()
	promRouter := promRouter()

	defer gock.Off()

	gock.New("http://foo.com").
		Get("/").
		Reply(200).
		JSON(map[string]string{"foo": "bar"})

	postBody := map[string]interface{}{
		"url": "http://foo.com",
	}
	body, _ := json.Marshal(postBody)

	postResponse := map[string]interface{}{
		"url":  "http://foo.com",
		"code": float64(200),
	}

	w := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodPost, "/", bytes.NewReader(body))
	assert.Nil(t, err)
	router.ServeHTTP(w, req)

	promW := httptest.NewRecorder()
	promReq, err := http.NewRequest(http.MethodGet, "/metrics", nil)
	assert.Nil(t, err)
	promRouter.ServeHTTP(promW, promReq)

	var response map[string]interface{}
	err = json.Unmarshal(w.Body.Bytes(), &response)
	assert.Nil(t, err)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, postResponse, response)

	assert.Equal(t, 200, promW.Code)

	var parser expfmt.TextParser
	mf, err := parser.TextToMetricFamilies(strings.NewReader(promW.Body.String()))
	assert.Nil(t, err)
	for k, v := range mf {
		if k == "http_get" {
			assert.Equal(t, 1, len(v.Metric))
			assert.Equal(t, 2, len(v.Metric[0].GetLabel()))
			for _, l := range v.Metric[0].GetLabel() {
				n := *l.Name
				v := *l.Value
				if n == "code" {
					assert.Equal(t, "200", v)
				} else if n == "url" {
					assert.Equal(t, "http://foo.com", v)
				}
			}
			assert.Equal(t, float64(1), v.Metric[0].GetCounter().GetValue())
		}
	}
}
