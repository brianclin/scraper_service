package main

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"golang.org/x/sync/errgroup"

	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
)

var (
	g        errgroup.Group
	logger   *zap.Logger
	httpReqs = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_get",
			Help: "How many HTTP requests processed, partitioned by request url and status code.",
		},
		[]string{"url", "code"},
	)
)

type ScraperRequest struct {
	URL string `json:"url"`
}

func initalizeLogger() {
	logger, _ = zap.NewProduction()
}

func postURL(c *gin.Context) {
	var newScraperRequest ScraperRequest

	if err := c.BindJSON(&newScraperRequest); err != nil {
		logger.Error("invalid request", zap.Error(err))
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "invalid request"})
	}

	resp, err := http.Get(newScraperRequest.URL)
	if err != nil {
		msg := fmt.Sprintf("could not get response from %s", newScraperRequest.URL)
		logger.Error(msg, zap.Error(err))
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": msg})
	}

	requestCounter := httpReqs.WithLabelValues(newScraperRequest.URL, strconv.Itoa((resp.StatusCode)))
	requestCounter.Inc()

	c.IndentedJSON(http.StatusOK, gin.H{"url": newScraperRequest.URL, "code": resp.StatusCode})
}

func router() http.Handler {
	e := gin.New()
	e.Use(ginzap.Ginzap(logger, time.RFC3339, true))
	e.Use(ginzap.RecoveryWithZap(logger, true))
	e.POST("/", postURL)

	return e
}

func promRouter() http.Handler {
	e := gin.New()
	e.Use(ginzap.Ginzap(logger, time.RFC3339, true))
	e.Use(ginzap.RecoveryWithZap(logger, true))
	e.GET("/metrics", prometheusHandler())

	return e
}

func prometheusHandler() gin.HandlerFunc {
	h := promhttp.Handler()

	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}

func init() {
	initalizeLogger()
	err := prometheus.Register(httpReqs)
	if err != nil {
		logger.Fatal("failed to register prometheus metric", zap.Error(err))
	}
}

func main() {
	server := &http.Server{
		Addr:         "0.0.0.0:8080",
		Handler:      router(),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	promServer := &http.Server{
		Addr:         "0.0.0.0:9095",
		Handler:      promRouter(),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	g.Go(func() error {
		return server.ListenAndServe()
	})

	g.Go(func() error {
		return promServer.ListenAndServe()
	})

	if err := g.Wait(); err != nil {
		logger.Fatal("server failed", zap.Error(err))
	}
}
