data "external" "tag" {
  program = ["sh", "get_tag.sh"]
}

resource "helm_release" "nginx_ingress" {
  name             = "nginx-ingress-controller"
  namespace        = "nginx"
  create_namespace = true

  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  version    = "4.6.0"

  values = [
    "${file("${path.module}/../k8s/nginx/values.yaml")}"
  ]
}

resource "helm_release" "prometheus" {
  name             = "prometheus"
  namespace        = "prometheus"
  create_namespace = true

  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "prometheus"
  version    = "22.6.2"
}

resource "helm_release" "scraper_service" {
  name             = "scraper-service"
  namespace        = "scraper-service"
  create_namespace = true
  chart            = "${path.module}/../k8s/service"

  set {
    name  = "image_tag"
    value = data.external.tag.result.tag
  }

  set {
    name  = "image_name"
    value = "scraper_service"
  }
}
