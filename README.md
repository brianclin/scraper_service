# scraper_service

Simple service that grab the HTTP GET code of a given URL and exposes a Prometheus metric

## Getting Started

To build the binary run `make build`

### Prerequisites
- terraform
- svu
- minikube
- asdf

### Installing

Add and install the plugins in `.tool-versions` using `asdf`

### Image builds

The images are built on merge to main and deployed to the repository in gitlab. The images are tagged with semantic versioning produced from the commit messages using `svu`

## Running the tests

`make test`

### Test script

There is a useful script in `scripts/`

This script takes in two arguments. The first is the url you want to do the HTTP GET on and the second is the number of times to do the HTTP GET. By default it assumes the scraper service is running on `localhost:8080`, but it can be overridden with the environment variable `SCRAPER_SERVICE_URL`

`./scripts/post_scraper_service http://google.com 2`

`SCRAPER_SERVICE_URL=http://scraper-service.local ./scripts/post_scraper_service http://google.com 2`

## Deployment

1. `minikube start`
2. `minikube addons enable ingress`
3. `make deploy`
4. Add `127.0.0.1 scraper-service.local` to `/etc/hosts`
5. `minikube tunnel`
6. You can now curl the scraper_service

### Make POST request

```
curl --request POST \
  --url http://scraper-service.local \
  --data '{
	"url":https://google.com"
}'
```

### Get Prometheus metrics
```
curl --request GET \
  --url http://scraper-service.local/metrics
```

## Docker

### Building

`make docker-build`

### Running

`make docker-run`

## Prometheus

Allows you to access prometheus server at `http://localhost:9090`
`kubectl port-forward svc/prometheus-server -n prometheus 9090:80`

### Queries

Get list of codes with an associated url

`sum by (code, url) (http_get)`

Get list of codes for a specific url (in this example it is google.com)

`sum by (code, url) (http_get{url="https://google.com"})`

Get all 2XX codes

`sum by (code, url) (http_get{code=~"2.."})`

Get all non 4xx codes

`sum by (code, url) (http_get{code!~"4.."})`
