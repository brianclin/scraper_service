BINARY_NAME=scraper_service

build:
	mkdir -p bin
	go build -o bin/${BINARY_NAME}

clean: ## Remove build related file
	rm -fr ./bin

run: build
	./bin/${BINARY_NAME}

test:
	go test ./...

test-coverage:
	go test ./... -coverprofile=coverage.out
	go tool cover -html=coverage.out -o coverage.html
	open coverage.html

docker-build:
	docker build -t $(BINARY_NAME) -f docker/Dockerfile .

docker-run: docker-build
	docker run -it -p 8080:8080 -p 9095:9095 --rm ${BINARY_NAME}

deploy:
	git fetch --tags
	terraform -chdir=terraform apply
